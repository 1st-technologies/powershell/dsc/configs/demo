Param(
    [string]$WorkingDir,
    [string]$ConfigName
)


Describe 'Pester-Test' {
    BeforeAll { 
        . ".\$ConfigName.ps1"
    }

    It 'Passes Script Analyzer' {
        Invoke-ScriptAnalyzer -Path "$WorkingDir\$ConfigName.ps1" -Severity Error | Should -BeNullOrEmpty
    }

    It 'Generated MOF' {
        "$WorkingDir\$ConfigName\localhost.mof" | Should -Exist
    }

    AfterAll {
        If(Test-Path -Path "$WorkingDir\$ConfigName\localhost.mof"){
            Remove-Item -Path "$WorkingDir\$ConfigName\localhost.mof"
        }
    }
}