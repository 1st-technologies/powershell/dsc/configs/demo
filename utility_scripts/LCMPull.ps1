[CmdletBinding()]
param(
    [Parameter(Mandatory = $true)]
    [ValidateNotNullOrEmpty()]
    [string]$Pullserver,
    [Parameter(Mandatory = $true)]
    [ValidateNotNullOrEmpty()]
    [string]$Key
)

[DSCLocalConfigurationManager()]
configuration LCMPull{
    Node localhost{
        Settings{
            RefreshMode                    = 'Pull'
            RefreshFrequencyMins           = 30
            ConfigurationModeFrequencyMins = 15
            ActionAfterReboot              = "ContinueConfiguration"
            ConfigurationMode              = "ApplyAndAutoCorrect"
            RebootNodeIfNeeded             = $false
            AllowModuleOverwrite           = $true
        }

        ConfigurationRepositoryWeb pullserver{
            ServerURL          = "$Pullserver/PSDSCPullServer.svc"
            RegistrationKey    = $Key
            ConfigurationNames = @('Demo')
        }   

        ReportServerWeb pullserver{
            ServerURL       = "Pullserver/PSDSCPullServer.svc"
            RegistrationKey = $Key
        }
    }
}

LCMPull
Set-DscLocalConfigurationManager -ComputerName localhost -Path .\LCMPull
