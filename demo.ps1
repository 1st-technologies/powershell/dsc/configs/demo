Configuration demo{
    Import-DscResource -ModuleName PSDesiredStateConfiguration
    
    Node localhost{

        # Set Services
        File HelloWorld {
            DestinationPath = "C:\Temp\HelloWorld.txt"
            Ensure = "Present"
            Contents   = "Hello World from DSC!"
        }
    }
}

demo