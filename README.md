[[_TOC_]]

# Prerequisites

Readers of this document are assumed to have working knowledge of PowerShell, Windows Server and Git. If you are not familiar with the latter, see this [git primer](https://danielmiessler.com/study/git/) before proceeding.

# Overview

This project is part of an overall scheme to manage Windows Servers using PowerShell DSC, Microsoft declarative configuration language. In this model, DSC "configurations" are maintained in Gitlab and published to a ["Pull Server"](https://learn.microsoft.com/en-us/powershell/dsc/pull-server/enactingconfigurations?view=dsc-1.1#pull-mode) in support of an [Infrastructure as Code](https://en.wikipedia.org/wiki/Infrastructure_as_code) model.

```mermaid
%%{ init: { 'flowchart': { 'curve': 'monotoneY' } } }%%
flowchart LR
    code(Code)
    vc(Version<br />Control)
    ci((Continuous<br />Integration))
    nuget(Powershell Gallery/<br />Nuget)
    winget(Chocolatey/<br />Winget)
    runner[Runner Service]
    ps(Pull Server)
    servers[[<br />&nbspServers&nbsp<br />&nbsp]]

    subgraph run[Gitlab]
        vc
        ci
    end

    subgraph runnerps[Gitlab Runner]
        runner
        ps
    end

    code -- commit --> vc
    vc --> ci
    ci ---> runner
    runner -- Merge --> ps
    winget -- Software --> servers
    ps -- Configuration --> servers
    nuget -- Modules --> ps
```

Alternatively, it is possible to modify the "run" stage of the pipeline ([.gitlab-ci.yml](.gitlab-ci.yml)) to support an entirely cloud-based environment using either [Azure Automation State Configuration](https://docs.microsoft.com/en-us/azure/automation/automation-dsc-overview) or [AWS Systems Manager](https://aws.amazon.com/about-aws/whats-new/2018/11/maintain-desired-state-configuration-and-report-compliance-of-windows-instances-using-aws-systems-manager-and-powershell-dsc/), which both have native support for PowerShell DSC.

```mermaid
%%{ init: { 'flowchart': { 'curve': 'monotoneX' } } }%%
flowchart LR
    code(Code)
    vc(Version<br />Control)
    ci((" Continuous<br />Integration "))
    nuget(Powershell Gallery/<br />Nuget)
    runner[[Gitlab<br />Runner]]
    servers[[<br />&nbspServers&nbsp<br />&nbsp]]
    clouddsc[[AWS/<br />Azure]]
    winget(Chocolatey/<br />Winget)

    subgraph run[Gitlab]
        vc
        ci
    end

    code -- Commit --> vc
    vc --> ci
    ci ---> runner
    runner -- Merge --> clouddsc
    nuget -- Modules --> clouddsc
    winget -- Software --> servers
    clouddsc -- Configuration --> servers
```

## PowerShell DSC v Ansible, Fight!

Why use DSC when Ansible has native support for Windows? A few reasons...

* DSC includes self-healing and drift management
* DSC includes a [RESTful report service](https://learn.microsoft.com/en-us/powershell/dsc/pull-server/reportserver?view=dsc-1.1) to query cached system compliance data
* DSC has more [Windows modules](https://www.powershellgallery.com/packages?q=Tags%3A%22DSC%22) than Ansible
* DSC can leverage the full PowerShell language; no need to learn jinja for advanced structures.
* DSC is Highly-Available by default since configurations are locally cached, scheduled and executed.
* DSC Pull Service supports automatic dependency management
* DSC configuration tasks support cross-host dependencies
* DSC includes a scheduler

Ansible does however have superior text file management

|While both Puppet and Ansible have "DSC" modules, neither leverage the local configuration manager and thus cannot perform self healing, caching or scheduling.|
|:--|

# Continuous Integration

Continuous Integration (CI) is the practice of streamlining the testing, compilation, merging and deployment of source code to end-points. GitLab combines Version Control and CI in one service.

In this project, CI techniques are employed to automate several of the manual functions involved in PowerShell DSC deployments such as managing modules dependencies, complication and distribution of MOF and CRC files.

In gitlab, CI is mediated by the .gitlab-ci.yml file. A simple .gitlab-ci.yml might look like the following.

```yml
stages:
  - dothething

run_dothething:
  stage: dothething
  tags:
    - im-a-tag  # <-- WHERE to run
    # tags are assigned to gitlab runners when installed,
    # setting a tag in a stage tells gitlab to only run the
    # stage on runners with matching tags; tags can be used to
    # designate OS, executor type and/or software dependencies.
  script: | # <-- WHAT to run
    # Configure Environment
    Do-TheThing

  only: # <-- WHEN to run (old way); this stage will only run on
  # certain conditions such a when merging to master branch
  # (i.e. ready for production)
    - master

  rules: # <-- WHEN to run (new way); rules are way more flexible.
    - if: '$CI_COMMIT_REF_NAME == "master"'
```

A Gitlab pipeline can consist of as many stages as needed. Furthermore,each stage can have unique rules. For instance, you can run testing stages for every commit but only run deployment stages when committing to the master branch.

The [.gitlab-ci.yml](.gitlab-ci.yml) file in this project has three stages; "dependencies", "test" and "run".

The "dependencies" stage reads the DSC configuration and will install required PowerSHell modules on the pullserver and package the modules for distribution to the end-points via the Pull Server.

The "test" stage tests for syntax errors then attempts to compile the configuration and reports errors. This prevents accidental deployment of malformed configurations.

Lastly, the "run" stage will compile and stage the configuration on the Pull Server. Afterwards, all subscribed servers will apply the new configuration the next time they check in with the Pull Server.

# Build

## Pull Server

1. Build or designate a new Windows Server to be the new Pull Server/Gitlab Runner, ensure that you have PowerShell 5.1 installed.
1. Open a PowerShell ISE prompt as admin and execute `Install-Module –Name xPSDesiredStateConfiguration, Pester, PSScriptAnalyzer`.
1. Optimally, generate a DNS alias (CNAME record) for your Pull Server.
1. Get an SSL certificate for the DSC Pull server from a trusted Certificate Authority or your internal PKI infrastructure and install the certificate in CERT:\LocalMachine\My. Make a note of the certificate thumbprint.
1. In the PowerShell prompt type `New-Guid()`, note the resulting GUID.
1. Open a new PowerShell ISE tab and paste in the following script, substituting the thumbprint, registration key and local configuration temp folder with those noted above.
    ```powershell
    configuration xDscPullServer{
        param(
            [string[]]$NodeName = 'localhost',
            [Parameter(Mandatory)]
            [ValidateNotNullOrEmpty()]
            [string] $certificateThumbPrint,
            [Parameter(Mandatory)]
            [ValidateNotNullOrEmpty()]
            [string] $RegistrationKey
        )
        Import-DSCResource -ModuleName xPSDesiredStateConfiguration
        Import-DSCResource –ModuleName PSDesiredStateConfiguration
        Node $NodeName{
            WindowsFeature DSCServiceFeature{
                Ensure = 'Present'
                Name = 'DSC-Service'
            }

        WindowsFeature Web-MgmtTools{
            Ensure = 'Present'
            Name = 'Web-Mgmt-Tools'
            DependsOn = '[WindowsFeature]DSCServiceFeature'
        }

        xDscWebService pullserverdev{
            Ensure = 'Present'
            EndpointName = 'pullserverdev'
            Port = 8080
            PhysicalPath = "$($env:SystemDrive)\inetpub\pullserverdev"
            CertificateThumbPrint = $certificateThumbPrint
            ModulePath = "$($env:PROGRAMFILES)\WindowsPowerShell\DscService\Modules"
            ConfigurationPath = "$($env:PROGRAMFILES)\WindowsPowerShell\DscService\Configuration"
            State = 'Started'
            DependsOn = '[WindowsFeature]DSCServiceFeature'
            UseSecurityBestPractices = $false
            RegistrationKeyPath = "$($env:ProgramFiles)\WindowsPowerShell\DscService"
            AcceptSelfSignedCertificates = $true
        }

        File RegistrationKeyFile{
            Ensure = 'Present'
            Type = 'File'
            DestinationPath = "$($env:ProgramFiles)\WindowsPowerShell\DscService\RegistrationKeys.txt"
            Contents = $RegistrationKey
        }
    }

    If(Test-Path -Path "$($env:Temp)\PullServer" -eq $False){
        New-Item -ItemType Directory -Path "$($env:Temp)\PullServer" |Out-Null
    }
    xDSCPullServer -certificateThumbprint '[CERTIFICATE THUMBPRINT]' -RegistrationKey '[REGISTRATION KEY]' -OutputPath "$($env:Temp)\PullServer"

    Start-DscConfiguration -Path "$($env:Temp)\PullServer" -Wait -Verbose
    ```
1. Reboot
1. Confirm the installation by navigating to https://[SERVER HOSTNAME]:8080/PSDSCPullServer.svc.

### High-Availability and Capacity Planning
The nature of DSC means that load balancing and redundancy are not strictly required given that all configurations are cached on the target nodes. A failure of a Pull Server only limits the deployment of new configurations until a new Pull Server can be brought online, which can be accomplished within minutes (assuming configurations are maintained in version control).

A single Pull Server using the internal MDB database can support up to 350 end-points or 3500 nodes when configured to [use Microsoft SQL server](https://techcommunity.microsoft.com/t5/core-infrastructure-and-security/configuring-a-powershell-dsc-web-pull-server-to-use-sql-database/ba-p/259626).

If you need to manage a dispersed environment and network considerations mandate that configuration data be closer to your nodes, You can employ [IIS Application Request Routing](https://www.iis.net/downloads/microsoft/application-request-routing) and [Distributed availability groups](https://learn.microsoft.com/en-us/sql/database-engine/availability-groups/windows/distributed-availability-groups?view=sql-server-ver16) to create a geographically distributed, Highly-Available Pull Server "farm".

|There are two well-maintained open-source DSC Pull Server products, [Tug](https://github.com/powershellorg/tug) and [DSC-TRÆK](https://github.com/powershellorg/dsc-traek).|
|:--|

## Gitlab Runner

This project employs a [Gitlab Runner](https://docs.gitlab.com/runner/) with the "shell" executor installed directly on the DSC Pull Server. Separating these roles is easily achievable but outside the scope of this POC.

1. Push [demo.ps1](demo.ps1), [.gitlab-ci.yml](.gitlab-ci.yml), [test.ps1](test.ps1) and [.gitignore.yml](.gitignore) to a [new project in your Gitlab instance](https://docs.gitlab.com/ee/user/project/working_with_projects.html#create-a-project) named "demo".
1. In the new project, navigate to **Settings** > **CI/CD** and note the url and registration token.
    ![Fig. 1](images/1.png)
1. Download the [Gitlab Runner for Windows x64](https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-windows-amd64.exe) to the Pull Server, copy it to C:\Gitlab-Runner and rename the file to "gitlab-runner.exe"
1. Open a command prompt as admin, CD to C:\gitlab-runner and enter the following commands:
   1. `gitlab-runner.exe install`
   1. `git-lab-runner.exe start`
   1. `gitlab-runner.exe register`
1. When prompted, specify the URL noted above.:
   ```
   Enter the GitLab instance URL (for example, https://gitlab.com/):
   https://gitlab.com/
   ```
1. Specify the registration token noted above:
   ```
   Enter the registration token:
   XXXXXXXXXXXXXXXXXXXXXXXXXXXXX
   ```
1. Type `Windows Pull Server` as a description.:
   ```
   Enter a description for the runner:
   [myhost]: Windows PullServer
   ```
1. Specify tags `exec_shell,lib_powershell5,lib_docker,lib_pullserver,os_windows,arch_x86_64,loc_us`, substituting "env_**dev*" for your desired operational environment e.g. "dev", "uat", "prod", "test", etc...:
   ```
   Enter tags for the runner (comma-separated):
   exec_shell,lib_powershell5,lib_docker,lib_pullserver,os_windows,arch_x86_64,loc_us
   ```
   **NOTE**: Tags are freeform labels you attach to your runners and ýou typically won't see this many to designate runner dependencies. In a production environment, the runner would be as bare bones as possible and all dependencies would be put into a container. See [The Docker executor](https://docs.gitlab.com/runner/executors/docker.html) for more information.
1. Specify a maintenance note for the runner. This could include the number of CPU cores, location or environment, e.g. "dev", "prod", uat", etc...
   ```
   Enter optional maintenance note for the runner:
   test
   ```
1. Specify `shell` for the "executor".:
   ```
   Enter an executor: parallels, shell, ssh, virtualbox, docker+machine, docker-ssh+machine, docker, docker-ssh, instance, kubernetes, custom, docker-windows:
   shell
   ```
1. Ensure the "tags" section of all stages in the [.gitlab-ci.yml](.gitlab-ci.yml) file to reflect the tags specified above.
1. Type `gitlab-runner.exe stop`
1. Change the `[[runners]]` > `shell` value in c:\Gitlab-Runner\config.toml from "pwsh" to "powershell".
    ```toml
    ...
    [[runners]]
        name = "myRunner"
        url = "https://gitlab.com/"
        id = 00000000
        token = "xxxxxxxxxxxxxxxxxxxx"
        token_obtained_at = 1900-01-00T00:00:00Z
        token_expires_at = 0001-01-01T00:00:00Z
        executor = "shell"
        shell = "powershell"  ←←←
    ...
    ```
1. Type `gitlab-runner.exe start`

## Register End-Point

To register the end-point to pull configurations, you must configure the Local Configuration Manager (LCM).

1. On the end-point, open PowerShell ISE as admin and paste in the following script. Set $key to the registration key defined when building the pullserver OR download and deploy [LCMPull.ps1](utility_scripts/LCMPull.ps1) with the necessary command line parameters.
    ```powershell
    [CmdletBinding()]
    param(
        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [string]$Pullserver,
        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [string]$Key
    )

    [DSCLocalConfigurationManager()]
    configuration LCMPull{
        Node localhost{
            Settings{
                RefreshMode                    = 'Pull'
                RefreshFrequencyMins           = 30
                ConfigurationModeFrequencyMins = 15
                ActionAfterReboot              = "ContinueConfiguration"
                ConfigurationMode              = "ApplyAndAutoCorrect"
                RebootNodeIfNeeded             = $false
                AllowModuleOverwrite           = $true
            }

            ConfigurationRepositoryWeb pullserver{
                ServerURL          = "$Pullserver/PSDSCPullServer.svc"
                RegistrationKey    = $Key
                ConfigurationNames = @('demo') # <-- The configuration to pull
            }   

            ReportServerWeb pullserver{
                ServerURL       = "Pullserver/PSDSCPullServer.svc"
                RegistrationKey = $Key
            }
        }
    }

    LCMPull
    Set-DscLocalConfigurationManager -ComputerName localhost -Path .\LCMPull
    ```
2. If you configured your Pull Server for HTTPS, ensure the client trusts the Pull Server's web certificate.

## Pipeline

1. Modify the "only" parameters in the "run" stage in the [.gitlab-ci.yml](.gitlab-ci.yml) file to execute only for commits to the master or main branch, whichever is the default for your Gitlab instance.
    ```yml
    ...
    only:
        - web # <-- change to "master" or "main"
    ...
    ```

# Conclusion

Every system set up with the LCM configuration script above will be set to pull and enforce the "demo" configuration every 30 minutes. Once the configuration is applied, it will continue to be enforced even in the event of Pull Server failure.

The Pull Server can host a nearly unlimited number of configurations. To build your own, create a new DSC configuration project, ensuring that the project and configuration names match and use the [include](https://docs.gitlab.com/ee/ci/yaml/includes.html) parameter to call the .gitlab-ci.yml of this project.

If you need to create a configuration with embedded password see [Securing the MOF File](https://learn.microsoft.com/en-us/powershell/dsc/pull-server/securemof?view=dsc-1.1).

Alternatively you can move [.gitlab-ci.yml](.gitlab-ci.yml) to a separate "Templates" project and have all DSC configurations call it from there. All that is necessary is that the DSC configuration project name match the Configuration name specified by the "Configuration" parameter.
```powershell
Configuration demo{
    Import-DscResource -ModuleName PSDesiredStateConfiguration
...
```

|It is possible to register end-points created by [Terraform](https://www.terraform.io/) using the [remote-exec](https://developer.hashicorp.com/terraform/language/resources/provisioners/remote-exec) provisioner for a complete provisioning and configuration solution in code.|
|:--|

## A Note on Nuget

It's important to keep in mind that all source code must be vetted before introduction into a production environment, even from trusted sources such as PowerShell Gallery. For that reason a scanning proxy such as [JFrog Artifactory](https://jfrog.com/artifactory/) or [Sonatype Nexus Repository](https://www.sonatype.com/products/nexus-repository) should be employed. For more cost conscious environments, it is a fairly simple matter to [build up your own internal Nuget server using Visual Studio](https://learn.microsoft.com/en-us/nuget/hosting-packages/nuget-server) or deploy one of several [open-source Nuget projects](https://learn.microsoft.com/en-us/nuget/hosting-packages/overview).

If you wish to employ a private Nuget repository, uncomment and complete the "Register internal PS Repository" section of script in the "dependencies" stage of the [.gitlab-ci.yml](.gitlab-ci.yml) file.

# Additional Reading

* [Gitlab: The .gitlab-ci.yml file](https://docs.gitlab.com/ee/ci/yaml/gitlab_ci_yaml.html)
* [Microsoft: PSDesiredStateConfiguration v1.1](https://learn.microsoft.com/en-us/powershell/dsc/overview?view=dsc-1.1)
* [Microsoft: Hosting your own NuGet feeds](https://learn.microsoft.com/en-us/nuget/hosting-packages/overview)
* [Microsoft: What is Pester and Why Should I Care?](https://devblogs.microsoft.com/scripting/what-is-pester-and-why-should-i-care/)
